# nhs-cli

```bash
$ echo "Simplifies NHS attendance/service spreadsheet interactions by means of the command line."
```

## Installation

### Pre-built Binaries

Requires no dependencies!

Download a release from [here](https://gitlab.com/nbnhs/nhs-cli/tags).

### Build From Source

**NOTE:** Compatibility with Windows is untested and not known. Best to stick with POSIX-compliant (ish) systems for now (e.g. macOS, Linux, BSD)

1. Install Node.js for your system from [here](https://nodejs.org/en/download/).
2. Install Yarn for your system from [here](https://yarnpkg.com/en/docs/install).
2. Download a release from [here](https://gitlab.com/nbnhs/nhs-cli/tags).
3. Un-archive the release.
4. Open a Terminal and navigate to the release.
5. Run `make install`.

You should now have the `nhs` command working in your Terminal.
