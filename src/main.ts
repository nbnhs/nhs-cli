#! /usr/bin/env node

import excuse from "./commands/excuse";
import login from "./commands/login";
import roll from "./commands/roll";
import addMeeting from "./commands/add-meeting";
import checkOutput from "./cli";

const command = process.argv[2];
const args = process.argv.slice(3);

if (command === "excuse" && args.length) {
  for (const name of args) {
    checkOutput(
      excuse(name),
      `Excused ${name}`,
      `Failed to excuse ${name!}`,
      2
    );
  }
} else if (command === "login" && args.length) {
  checkOutput(
    login(args[0]),
    "Logged into Google Drive successfully!",
    "Couldn't log into Google Drive!",
    3
  );
} else if (command === "roll" && args.length) {
  for (const name of args) {
    checkOutput(
      roll(name),
      "",
      `Couldn't get attendance records for ${name}!`,
      5
    );
  }
} else if (command === "add-meeting" && args.length) {
  checkOutput(
    addMeeting(args[0]),
    `Added new meeting ${args[0]}`,
    `Failed to add meeting ${args[0]}!`,
    6
  );
} else {
  console.log(`Usage: nhs-cli [command] [options]

  Where "command" and "options" are one of:
  - login [credentials file] (logs you into GDrive)
  - roll [person name] (gets roll record for person)
  - add-meeting [date] (adds a new column before the Scripting Column with the first cell being the date)
  - excuse [person name] (excuses person from next meeting)

  Exit code meanings:
  - 1 = command not recognized
  - 2 = excuse failed
  - 3 = login failed
  - 4 = environment variable not set
  - 5 = roll failed
  - 6 = add-meeting failed
          `);
  process.exit(1);
}
