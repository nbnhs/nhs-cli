import { good, bad } from "./helpers/log";

/**
 * Checks the output of a function call (which should be a `Promise<boolean>`).
 * Displays the success message to the console in green if the Promise results in `true`.
 * Otherwise displays the failure message in red and exits with the exit code.
 * @param funcCall Function call to check the result of.
 * @param successMsg Message to display on success.
 * @param failureMsg Message to display on failure.
 * @param exitCode Code to exit with on failure.
 */
export default function checkOutput(
  funcCall: Promise<boolean>,
  successMsg: string,
  failureMsg: string,
  exitCode: number
): void {
  funcCall.then(val => {
    if (val) {
      good(successMsg);
    } else {
      bad(failureMsg);
      process.exit(exitCode);
    }
  });
}
