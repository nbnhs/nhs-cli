import { google, sheets_v4 } from "googleapis";
import { OAuth2Client } from "googleapis-common";
import { bad, info } from "../helpers/log";
import fetch from "node-fetch";
import { csv2arr } from "../helpers/getCSV";

/**
 * Represents a Google Sheet in Drive, providing functions to find rows and edit cells.
 */
export default class Sheet {
  public googleSheet: sheets_v4.Sheets;
  public sheet: Promise<string[][]>;

  constructor(
    private login: OAuth2Client,
    private id: string,
    private gid?: string
  ) {
    this.googleSheet = this.getSheetsObject(login);
    this.sheet = this.getCSV();
  }

  private getSheetsObject(auth: OAuth2Client) {
    return google.sheets({
      auth,
      version: "v4"
    });
  }

  /**
   * Converts a (row, col) location into A1 notation.
   * @param row Row number
   * @param col Column number
   * @returns String in the format "ROW_LETTER_COLUMN_NUMBER"
   */
  private toRangeString(row: number, col: number): string {
    return `${String.fromCharCode(97 + col)}${row}`;
  }

  /**
   * Downloads the Google Sheet at the given ID as a 2-dimensional string array.
   * @returns The sheet at `this.id` as a 2D string array
   */
  private async getCSV(): Promise<string[][]> {
    const res = await fetch(
      `https://www.googleapis.com/drive/v3/files/${
        this.id
      }/export?mimeType=text%2Fcsv`,
      {
        headers: {
          Authorization: `Bearer ${(await this.login.getAccessToken()).token}`,
          Accept: "application/json"
        }
      }
    );

    return await csv2arr(await res.text());
  }

  /**
   * Changes the cell located at (row, col) to the specified value
   * @param row Row number.
   * @param col Column number.
   * @param newValue New cell value.
   * @returns True if the change goes through; false otherwise
   */
  public async changeCellValue(
    row: number,
    col: number,
    newValue: string
  ): Promise<boolean> {
    const values = [[newValue]];

    const resource = { values };

    return new Promise<boolean>((resolve, _) => {
      this.googleSheet.spreadsheets.values.update(
        {
          range: this.toRangeString(row, col),
          requestBody: resource,
          spreadsheetId: this.id,
          valueInputOption: "USER_ENTERED"
        },
        (err: any, _: any) => {
          if (err) {
            bad(`Error from Sheets: ${err}`);
            resolve(false);
          } else {
            info(`Changed cell value`);
            resolve(true);
          }
        }
      );
    });
  }

  /**
   * Adds an empty column to the sheet, retaining text formatting/other properties.
   * @param before Numerical index of the column to insert the new one *before*.
   * @returns False if error, true otherwise.
   */
  public async addColumn(before: number): Promise<boolean> {
    const requests: any[] = [
      {
        insertDimension: {
          range: {
            sheetId: this.gid,
            dimension: "COLUMNS",
            startIndex: before,
            endIndex: before + 1
          },
          inheritFromBefore: true
        }
      }
    ];

    return new Promise((resolve, _) => {
      this.googleSheet.spreadsheets.batchUpdate(
        {
          spreadsheetId: this.id,
          requestBody: { requests }
        },
        (err: any, _: any) => {
          if (err) {
            bad(err);
            resolve(false);
          } else {
            resolve(true);
          }
        }
      );
    });
  }

  /**
   * Finds the row number for the person specified.
   * @param person Either the person's first, last, or full name.
   * @returns The row number corresponding to the person.
   */
  public async findRow(person: string): Promise<number> {
    const firstAndLastGiven: boolean = person.indexOf(" ") > -1;

    for (let i = 0; i < (await this.sheet).length; i++) {
      const firstName = (await this.sheet)[i][1].toLowerCase();
      const lastName = (await this.sheet)[i][2].toLowerCase();

      if (firstAndLastGiven) {
        const name: string[] = person.split(" "); // First, Last

        if (
          firstName === name[1].toLowerCase() &&
          lastName === name[0].toLowerCase()
        ) {
          return i + 1;
        }
      } else {
        if (
          firstName === person.toLowerCase() ||
          lastName === person.toLowerCase()
        ) {
          return i + 1;
        }
      }
    }

    bad(`Can't find ${person} anywhere in the Google Sheet!`);
    if (!firstAndLastGiven) {
      info("Try specifying both first and last names.");
    }
    return -1;
  }

  /**
   * Gets the numerical index of a desired column.
   * @param title The title (top-most cell) of the column to find.
   * @returns Numerical index of the desired column. -1 if not found.
   */
  public async findColumn(title: string): Promise<number> {
    return (await this.sheet)[0].findIndex(element => element === title);
  }

  /**
   * Gets the column number of the next meeting.
   * @returns Column number of next meeting.
   */
  public async getNextMeetingColumn(): Promise<number> {
    const row1: string[] = (await this.sheet)[0];
    return row1.indexOf("Scripting Column") - 1;
  }
}
