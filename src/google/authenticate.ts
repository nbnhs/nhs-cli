// Copyright (c) 2019 Milo Gilad
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

import * as fs from "fs";
import { google } from "googleapis";
import { good, info, bad } from "../helpers/log";
import { OAuth2Client } from "googleapis-common";
import userInput from "../helpers/userInput";
import { homedir } from "os";

const TOKEN_PATH = `${homedir()}/.config/nhs-cli/token.json`;
const CREDS_PATH = `${homedir()}/.config/nhs-cli/credentials.json`

/**
 * Get and store new token after prompting for user authorization (need only run once).
 * @param oAuth2Client The OAuth2 client to get token for.
 */
export function storeNewToken(oAuth2Client: OAuth2Client): void {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: "offline",
    scope: [
      "https://www.googleapis.com/auth/spreadsheets",
      "https://www.googleapis.com/auth/drive"
    ]
  });

  console.log("Authorize this app by visiting this url:", authUrl);
  const code: string = userInput("Enter the code from that page here: ");
  oAuth2Client.getToken(code, (err, token) => {
    if (err || !token) {
      bad(`Error while trying to retrieve access token: ${err}`);
      return;
    } else {
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), err => {
        if (err) {
          bad(`Couldn't store token to file: ${err}`);
          return;
        }
        info(`Token stored to ${TOKEN_PATH}`);
      });
    }
  });
}

/**
 * Construct an OAuth2Client from the given credentials file (usually credentials.json).
 * @param path Path to file to construct from
 * @returns Promise for an OAuth2Client constructed from file
 */
export async function oAuthFromFile(path: string): Promise<OAuth2Client> {
  const credentials = JSON.parse(String(fs.readFileSync(path)));
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  return new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
}

/**
 * Logs into Google Drive.
 *
 * This code is ripped directly from Google's "Getting Started" wiki. It just works.
 *
 * `token.json` (containing your credentials to Google Drive in their format) is expected within the current directory.
 */
export async function authorizeGoogle(): Promise<OAuth2Client> {
  const oAuth2Client = await oAuthFromFile(CREDS_PATH);

  return new Promise((resolve, reject) => {
    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) {
        bad("Couldn't open stored Google Drive token! Run nhs login.");
        reject(err);
      }
      oAuth2Client.setCredentials(JSON.parse(String(token)));
      good("Logged into Google Drive.");
      resolve(oAuth2Client);
    });
  });
}

