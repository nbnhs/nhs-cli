import chalk from "chalk";

/**
 * Runs specified function with specified parameter and prints result.
 * @param text Text to pass to fn.
 * @param fn Function that accepts a single string argument
 */
function formatOutput(text: string, fn: (txt: string) => void) {
    console.log(fn(text));
}

/**
 * Logs red text to the console
 * @param txt Text to log
 */
export function bad(txt: string) {
    formatOutput(txt, chalk.red);
}

/**
 * Logs yellow text to the console
 * @param txt Text to log
 */
export function warn(txt: string) {
    formatOutput(txt, chalk.yellow);
}

/**
 * Logs green text to the console
 * @param txt Text to log
 */
export function good(txt: string) {
    formatOutput(txt, chalk.green);
}

/**
 * Logs blue text to the console
 * @param txt Text to log
 */
export function info(txt: string) {
    formatOutput(txt, chalk.blue);
}
