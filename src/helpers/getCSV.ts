import { parse } from "papaparse";

/**
 * Turns a CSV (Comma-separated values) file into a 2-dimensional array with the Papaparse library.
 * @param s The CSV to interpret into a string array.
 */
export async function csv2arr(s: string): Promise<string[][]> {
  return new Promise((resolve, reject) => {
    parse(s, {
      complete: results => {
        resolve(results.data);
      },
      error: error => {
        reject({
          status: error,
          statusText: error.message
        });
      }
    });
  });
}
