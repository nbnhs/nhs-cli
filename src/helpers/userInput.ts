import readline from "readline-sync";

export default function userInput(prompt: string): string {
  return readline.question(prompt);
}
