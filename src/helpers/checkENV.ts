import { bad, info } from "./log";

export default function checkENV(variable: any, example: string): boolean {
    if (!process.env[variable]) {
        bad(`You haven't set the ${variable} environment variable!`);
        info(
          `For example: ${example}`
        );
        return false;
    }

    return true;
}
