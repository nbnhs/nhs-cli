import { authorizeGoogle } from "../google/authenticate";
import Sheet from "../google/Sheet";
import checkENV from "../helpers/checkENV";

/**
 * Gets the attendance records for the specified person.
 * @param person Person to get attendance for.
 * @returns String array in the form `["Date: Present/Absent/Excused", ...]`
 */
export default async function roll(person: string): Promise<boolean> {
  if (
    !checkENV(
      "NHS_ROLL_ID",
      "In the URL https://docs.google.com/spreadsheets/d/IDNUMBER/edit, the ID is IDNUMBER"
    )
  ) {
    process.exit(4);
  }

  const sheet = new Sheet(
    await authorizeGoogle(),
    String(process.env.NHS_ROLL_ID)
  );

  const firstRow = (await sheet.sheet)[0];
  const personRow = (await sheet.sheet)[(await sheet.findRow(person)) - 1];
  const result: string[] = [];
  for (let i = 0; i < firstRow.length; i++) {
    result.push(`${firstRow[i]}: ${personRow[i]}`);
  }

  console.log(result);
  return true;
}
