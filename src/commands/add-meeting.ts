import Sheet from "../google/Sheet";
import { authorizeGoogle } from "../google/authenticate";
import checkENV from "../helpers/checkENV";

/**
 * Adds a new meeting column immedietally before "Scripting Column".
 * @param date The date for the new meeting.
 * @returns True/false for success/failure
 */
export default async function addMeeting(date: string): Promise<boolean> {
  if (
    !checkENV(
      "NHS_ROLL_ID",
      "In the URL https://docs.google.com/spreadsheets/d/IDNUMBER/edit, the ID is IDNUMBER"
    ) ||
    !checkENV(
      "NHS_ROLL_GID",
      "In the URL https://docs.google.com/spreadsheets/d/IDNUMBER/edit#gid=GID, the GID is GID."
    )
  ) {
    process.exit(4);
  }

  const sheet = new Sheet(
    await authorizeGoogle(),
    String(process.env.NHS_ROLL_ID),
    String(process.env.NHS_ROLL_GID)
  );
  const before = await sheet.findColumn("Scripting Column");
  if (!(await sheet.addColumn(before))) {
    return false;
  }

  return await sheet.changeCellValue(1, before, date);
}
