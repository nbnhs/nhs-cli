import userInput from "../helpers/userInput";
import Sheet from "../google/Sheet";
import { authorizeGoogle } from "../google/authenticate";
import checkENV from "../helpers/checkENV";

/**
 * Marks "name" as excused for the next meeting. Also prompts for a reason for excuse.
 * @param name Person to excuse.
 */
export default async function excuse(name: string): Promise<boolean> {
  const reason = userInput("Reason for absence: ");
  if (
    !checkENV(
      "NHS_ROLL_ID",
      "In the URL https://docs.google.com/spreadsheets/d/IDNUMBER/edit, the ID is IDNUMBER"
    )
  ) {
    process.exit(4);
  }

  const sheet = new Sheet(
    await authorizeGoogle(),
    String(process.env.NHS_ROLL_ID)
  );

  const cellValue = `Excused ${reason}`;
  const cell: number[] = [
    await sheet.findRow(name),
    await sheet.getNextMeetingColumn()
  ];
  return await sheet.changeCellValue(cell[0], cell[1], cellValue);
}
