import { oAuthFromFile, storeNewToken } from "../google/authenticate";
import * as fs from "fs";
import { homedir } from "os";

/**
 * Logs the user into Google Drive/Sheets and stores the credentials and token files inside a directory in ~/.config
 * @param credsFilePath Path to the credentials.json file obtained from Google.
 */
export default async function login(credsFilePath: string): Promise<boolean> {
  if (!fs.existsSync(`${homedir()}/.config/nhs-cli`)) {
    fs.mkdirSync(`${homedir()}/.config/nhs-cli`);
  }

  storeNewToken(await oAuthFromFile(credsFilePath));

  fs.copyFileSync(
    credsFilePath,
    `${homedir()}/.config/nhs-cli/credentials.json`
  );
  return true;
}
