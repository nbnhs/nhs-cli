build: deps
	npx tsc

deps:
	yarn

lint: deps
	npx tslint -c tslint.json 'src/**/*.ts'

run: build
	node .

install: build
	chmod +x out/main.js
	yarn link

docs: deps
	npx typedoc --out public src